from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'voucher/index.html')

def create(request):
    return render(request, 'voucher/create.html')