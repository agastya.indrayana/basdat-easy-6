from django.urls import path

from . import views

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('registrasi/', views.register, name='index'),
    path('login/', views.login, name='login'),
    path('topup/', views.topup, name='topup'),
    path('riwayat/', views.transactionHistory, name='riwayat'),
    path('laporan/', views.reportList, name='reportList'),
    path('api/add_person/', views.api_add_person, name='adding person logic'),
    path('api/logout/', views.api_logout, name='logout logic'),
    path('api/login/', views.api_login, name='login logic'),
    path('api/topup/', views.api_topup, name='topup logic'),
]
