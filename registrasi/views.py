from django.shortcuts import render, redirect
from django.http import HttpResponse
# from registrasi.models import Person
# from registrasi.forms import PersonForm


def homepage(request):
    return render(request, 'common/homepage.html')


def register(request):
    return render(request, 'registrasi/register.html')


def login(request):
    return render(request, 'registrasi/login.html')


def topup(request):
    return render(request, 'registrasi/topup.html')


def transactionHistory(request):
    return render(request, 'registrasi/transactionHistory.html')


def reportList(request):
    return render(request, 'registrasi/reportList.html')


def api_add_person(request):
    # TODO: implement checking and registration logic
    KTPNumber = request.POST['KTPNumber']
    name = request.POST['name']
    email = request.POST['email']
    birthdate = request.POST['birthdate']
    phoneNumber = request.POST['phoneNumber']
    address = request.POST['address']
    role = request.POST['role']

    request.session["role"] = request.POST['role']
    request.session["name"] = request.POST['name']
    request.session["loged_in"] = True
    return HttpResponse("hello")


def api_login(request):
    # TODO: implement login logic
    return HttpResponse("hello")


def api_logout(request):
    request.session.flush()
    return HttpResponse("You've been loged out")


def api_topup(request):
    # TODO: implement topup logic
    return HttpResponse("You are topping up")
