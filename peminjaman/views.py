from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, 'peminjaman/index.html')

def create(request):
    return render(request, 'peminjaman/create.html')
